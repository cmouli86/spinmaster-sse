const logger = require('../utils/Logger');
const config = require('../config/configuration.js');
const orderService = require('../orderService/OrderService.js');
const responseGenerator = require('../utils/ResponseGenerator');

module.exports =  (app)=>{

  app.get('/v1/testOrderSE', async function(req, res){
    'use strict';

    logger.debug ("==================>Inside /v1/testOrderSE");
    res.status(200).json("API working");
});


    app.post('/v1/orderSubmit/jsonToXML', async function(req, res){
        'use strict';
     try {
       logger.debug ("==================>Inside /v1/orderSubmit/jsonToXML");
       //logger.debug ("==================>req body="+JSON.stringify(req.body));
       await orderService.createOrderXML(req,res);
      } catch (error) {
        logger.error("Error while converting and writing json to xml. ");
        //res.status(400).json("Internal Error"+error);
      }
    });

    app.post('/v1/orderStatus/update',  async function(request, response){
      try {
        logger.debug ("==================>Inside /v1/orderStatus/update");
       await orderService.updateOrderStatus(request,response);
      } catch (e) {
          logger.error("Error while update order status process");
          //res.status(400).json("Internal Error"+error);
      }
      return;
    });
}
