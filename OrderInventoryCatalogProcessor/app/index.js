const express = require('express');
var bodyParser = require('body-parser');
var logger = require('../utils/Logger');
var app = module.exports = express();
var xmlparser = require('express-xml-bodyparser');

app.use(function(req,res,next){
	logger.setLogger(res.locals.logger);
	next();
});


app.use(xmlparser());
app.use(bodyParser.json({strict: false}));

require('./OrderController')(app);
require('./CatalogController')(app);
module.exports = app;
