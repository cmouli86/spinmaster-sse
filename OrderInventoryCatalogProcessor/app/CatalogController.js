const logger = require('../utils/Logger');
const catalogService = require('../catalogService/CatalogService.js');

module.exports =  (app)=>{

  app.get('/v1/testCatalogSE', async function(req, res){
    'use strict';

    logger.debug ("==================>Inside /v1/testCatalogSE");
    res.status(200).json("API working");
});


    app.post('/v1/inventory/update', async function(req, res){
        'use strict';
     try {
       logger.debug ("==================>Inside /v1/inventory/update");
       //logger.debug ("==================>req body="+JSON.stringify(req.body));
       await catalogService.updateInventoryFromFile(req,res);
      } catch (error) {
        logger.error("Error during inventory update ");
       // res.status(400).json("Internal Error"+error);
      }
    });

    app.post('/v1/price/update', async function(req, res){
      'use strict';
   try {
     logger.debug ("==================>Inside /v1/price/update");
     //logger.debug ("==================>req body="+JSON.stringify(req.body));
     await catalogService.updatePriceFromFile(req,res);
    } catch (error) {
      logger.error("Error during price update ");
      //res.status(400).json("Internal Error"+error);
    }
  });
}
