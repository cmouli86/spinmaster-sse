const orderXMLFileLocation = "/u01/bridge/data_xml/inbound/order_submit_es/";
const orderXMLFileName = "order_submit_es_";
const orderStatusFileLocation = "/u01/bridge/data_xml/outbound/order_status_es/";
const orderStatusArchiveLocation = "/u01/bridge/archive/outbound/order_status_es/";
const orderStatusErrorLocation = "/u01/bridge/error/outbound/order_status_es/";
const occOrderURL = "/ccadmin/v1/orders/";
const usSiteId = "700001";
const caSiteId = "700004";
const sapShipToNo = "EMPLOYEE";
const sapBillToNo = "EMPLOYEE";
const usCreditCardCode = "Z103";
const caCreditCardCode = "Z104";
const paymentType = "creditCard";
const quantityType = "PC";
const thirdPartyShippingContract = 0;
const shippingCondition = "ST";
const orderSite = "ES";
const usSite = "US";
const caSite = "CA";
const xmlEncoding = "UTF-8";
const xmlStandAlone = "yes";
const xmlVersion = "1.0";
const shipped = "Shipped";
const cancelled = "Cancelled";
const orderShippedState = "NO_PENDING_ACTION";
const shipGroupShippedState = "NO_PENDING_ACTION";
const orderCancelledState = "REMOVED";
const shipGroupCancelledState = "REMOVED";
const orderProcessingState = "PROCESSING";
const submittedState = "SUBMITTED";
const orderSubmitXMLFileNameDateFormat = "yyyyMMddhhmmss";
const orderSubmitDateFormat = "yyyy-MM-dd hh:mm:ss";
const orderStatusDateFormat = "yyyy-MM-dd hh:mm:ss";
const orderSubmitJsonDateFormat = "yyyy-MM-ddThh:mm:ss.SSSZ";
const orderStatusJsonDateFormat = "yyyy-MM-ddThh:mm:ss.SSSZ";
const inventoryFileProcessLocation="/u01/bridge/data_xml/outbound/inventory/";
const inventoryFileArchiveLocation="/u01/bridge/archive/outbound/inventory/";
const priceFileProcessLocation="/u01/bridge/data_xml/outbound/price/";
const priceFileArchiveLocation="/u01/bridge/archive/outbound/price/";
const priceFileErrorLocation="/u01/bridge/error/outbound/price/";
const inventoryUSSiteInFile="ESUS";
const inventoryCASiteInFile="ESCA";
const US="US";
const CA = "CA";
const priceUSRegionInFile="US";
const priceCARegionInFile="US";
const inventoryLocationIdUS="employee-us";
const inventoryLocationIdCA="employee-ca";
const priceListIdUS="employee_us_price_grp";
const priceListIdCA="employee_ca_price_grp";
const inventoryURL = "/ccadmin/v1/inventories/";
const priceURL = "/ccadmin/v1/skus/";
 
var OCCConfig = {
    port : 443,
    hostname : "ccadmin-prod-z5na.oracleoutsourcing.com",
    adminUrl : "ccadmin-prod-z5na.oracleoutsourcing.com",
    storeUrl : "ccstore-prod-z5na.oracleoutsourcing.com",
    apiKey : "eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJzdWIiOiIwMzA3MTg4MC01NTJlLTRmZTYtYTA3NS1iNmQwMTRkNDE0NDYiLCJpc3MiOiJhcHBsaWNhdGlvbkF1dGgiLCJleHAiOjE2MDYyNDM0MzYsImlhdCI6MTU3NDcwNzQzNn0=.tr4E8vUS1IaeCC7Z8wb06ncMMKyUKWy/sHOjeICRiIs=",
    proxyAgent :null
}
module.exports = {
    "xmlFileLocation": orderXMLFileLocation,
    "xmlFileName": orderXMLFileName,
    orderStatusFileLocation,
    "orderURL":occOrderURL,
    usSiteId, sapShipToNo, sapBillToNo,usCreditCardCode,caCreditCardCode,paymentType,quantityType,
    thirdPartyShippingContract,orderSite,shippingCondition,usSite,caSite,xmlEncoding,
    xmlStandAlone,xmlVersion,orderStatusArchiveLocation,orderStatusErrorLocation,
    orderShippedState,shipGroupShippedState,orderSubmitXMLFileNameDateFormat,orderSubmitDateFormat,
    orderStatusDateFormat,shipped,orderSubmitJsonDateFormat,orderStatusJsonDateFormat,
    inventoryFileProcessLocation,inventoryFileArchiveLocation,priceFileArchiveLocation,
    priceFileProcessLocation,inventoryUSSiteInFile,inventoryCASiteInFile,priceCARegionInFile,
    priceUSRegionInFile,US,CA,inventoryLocationIdUS,inventoryLocationIdCA,inventoryURL,
    priceURL,priceListIdCA,priceListIdUS,priceFileErrorLocation,cancelled,shipGroupCancelledState,
    orderCancelledState,caSiteId,orderProcessingState,submittedState,OCCConfig
   }
   