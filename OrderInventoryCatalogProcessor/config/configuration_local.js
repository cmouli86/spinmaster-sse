const orderXMLFileLocation = "D:/files/spinmaster/";
const orderXMLFileName = "order_submit_es_";
const orderStatusFileLocation = "D:/files/spinmaster/orderstatus/process/";
const orderStatusArchiveLocation = "D:/files/spinmaster/orderstatus/archive/";
const orderStatusErrorLocation = "D:/files/spinmaster/orderstatus/error/";
const occOrderURL = "/ccadmin/v1/orders/";
const usSiteId = "300001";
const caSiteId = "300003";
const sapShipToNo = "EMPLOYEE";
const sapBillToNo = "EMPLOYEE";
const usCreditCardCode = "Z103";
const caCreditCardCode = "Z104";
const paymentType = "creditCard";
const quantityType = "PC";
const thirdPartyShippingContract = 0;
const shippingCondition = "ST";
const orderSite = "ES";
const usSite = "US";
const caSite = "CA";
const xmlEncoding = "UTF-8";
const xmlStandAlone = "yes";
const xmlVersion = "1.0";
const shipped = "Shipped";
const cancelled = "Cancelled";
const orderShippedState = "NO_PENDING_ACTION";
const shipGroupShippedState = "NO_PENDING_ACTION";
const orderCancelledState = "REMOVED";
const shipGroupCancelledState = "REMOVED";
const orderProcessingState = "PROCESSING";
const submittedState = "SUBMITTED";
const orderSubmitXMLFileNameDateFormat = "yyyyMMddhhmmss";
const orderSubmitDateFormat = "yyyy-MM-dd hh:mm:ss";
const orderStatusDateFormat = "yyyy-MM-dd hh:mm:ss";
const orderSubmitJsonDateFormat = "yyyy-MM-ddThh:mm:ss.SSSZ";
const orderStatusJsonDateFormat = "yyyy-MM-ddThh:mm:ss.SSSZ";

const inventoryFileProcessLocation="D:/files/spinmaster/inventory/process/";
const inventoryFileArchiveLocation="D:/files/spinmaster/inventory/archive/";
const priceFileProcessLocation="D:/files/spinmaster/price/process/";
const priceFileArchiveLocation="D:/files/spinmaster/price/archive/";
const priceFileErrorLocation="D:/files/spinmaster/price/error/";
const inventoryUSSiteInFile="ESUS";
const inventoryCASiteInFile="ESCA";
const US="US";
const CA = "CA";
const priceUSRegionInFile="US";
const priceCARegionInFile="US";
const inventoryLocationIdUS="employee-us";
const inventoryLocationIdCA="employee-ca";
const priceListIdUS="employee_us_price_grp";
const priceListIdCA="employee_ca_price_grp";
const inventoryURL = "/ccadmin/v1/inventories/";
const priceURL = "/ccadmin/v1/skus/";
 

var OCCConfig = {
    port : 443,
    hostname : "ccadmin-test-z5na.oracleoutsourcing.com",
    adminUrl : "ccadmin-test-z5na.oracleoutsourcing.com",
    storeUrl : "ccstore-test-z5na.oracleoutsourcing.com",
    apiKey : "eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJzdWIiOiIzNjRjZmVlMy00MDEwLTRmNTUtOGI4OC0zYThkOGMyMTg4NzIiLCJpc3MiOiJhcHBsaWNhdGlvbkF1dGgiLCJleHAiOjE1ODQwNDEzMzAsImlhdCI6MTU1MjUwNTMzMH0=.zqIIOpmU9BiPWs3u9H+3hWF+iz1+ZGe+yXh4VWcFhUM=",
    proxyAgent :null
}

module.exports = {
    "xmlFileLocation": orderXMLFileLocation,
    "xmlFileName": orderXMLFileName,
    orderStatusFileLocation,
    "orderURL":occOrderURL,
    usSiteId, sapShipToNo, sapBillToNo,usCreditCardCode,caCreditCardCode,paymentType,quantityType,
    thirdPartyShippingContract,orderSite,shippingCondition,usSite,caSite,xmlEncoding,
    xmlStandAlone,xmlVersion,orderStatusArchiveLocation,orderStatusErrorLocation,
    orderShippedState,shipGroupShippedState,orderSubmitXMLFileNameDateFormat,orderSubmitDateFormat,
    orderStatusDateFormat,shipped,orderSubmitJsonDateFormat,orderStatusJsonDateFormat,
    inventoryFileProcessLocation,inventoryFileArchiveLocation,priceFileArchiveLocation,
    priceFileProcessLocation,inventoryUSSiteInFile,inventoryCASiteInFile,priceCARegionInFile,
    priceUSRegionInFile,US,CA,inventoryLocationIdUS,inventoryLocationIdCA,inventoryURL,
    priceURL,priceListIdCA,priceListIdUS,priceFileErrorLocation,cancelled,shipGroupCancelledState,
    orderCancelledState,caSiteId,orderProcessingState,submittedState,OCCConfig
   }
   