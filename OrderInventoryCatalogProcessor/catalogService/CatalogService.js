const logger = require('../utils/Logger');
const config = require('../config/configuration.js');
var commercesdk = require('../occ/occ-sdk');
var occClient = new commercesdk(config.OCCConfig);
const orderUtil = require('../utils/ProcessorUtil.js');
var async = require("async");

var catalogService = {
    updateInventoryFromFile:function(request,response){
        return new Promise(async function (resolve,reject) {
        logger.debug("in updateInventoryFromFile method");
        var responseArray = [];
        try{
        var fileNameJsonMap  = orderUtil.readJsonFromXMLFile(config.inventoryFileProcessLocation,true,config.inventoryFileArchiveLocation,null);
        logger.debug("fileNameJsonMap size "+fileNameJsonMap.size);
        if(fileNameJsonMap.size > 0){
            for (var entry of fileNameJsonMap.entries()){
                var fileName = entry[0];
                var result = entry[1];
                logger.debug("filename {},result {}",fileName,JSON.stringify(result));
                if(result.item != null){
                    if(result.item.length > 0){
                        logger.debug("multiple inventories in single file="+fileName);
                        for(const inventoryItem of result.item){
                            await new Promise(resolve => catalogService.updateInventoryStatusFromJson(inventoryItem, resolve, responseArray));
                        }
                    }else{
                        logger.debug("single inventories in file="+fileName);
                        await new Promise(resolve => catalogService.updateInventoryStatusFromJson(result.item, resolve, responseArray));
                   }
                }
            }
            logger.info("all inventory update proccess completed",responseArray);
            response.status(200).json(responseArray);
            resolve(response);
        }else{
            response.status(200).json([{"status":"success","inventoryId":"","message ":"no inventory files to process"}]);
            resolve(response);
        }
    }catch(ex){
        logger.error("error while updating inventories")
        logger.error(ex);
        response.status(400).json([{"status":"failure","inventoryId":"","message ":"error while updating inventories. reason:"+ex}]);
        reject(response);
    }
    });

  },
  updateInventoryStatusFromJson : function(inventoryJson, resolve, responseArray){
    var inventoryId = inventoryJson.$.materialNo;
    logger.debug("inventoryId="+inventoryId);
    var stockLevel = inventoryJson.quantityOnHand;
    var locationId = config.inventoryUSSiteInFile==inventoryJson.$.site?config.inventoryLocationIdUS:config.inventoryLocationIdCA;
    try{
        var inventoryData = {
            "locationId":locationId,
            "stockLevel":stockLevel
        }
        logger.debug("inventoryData=",inventoryData);
        occClient.put({
            url: config.inventoryURL+inventoryId,data: inventoryData,
            headers: {
                Accept: '*/*',
                Connection: 'keep-alive'			 
                },
            callback:function(error,res){
                if(error){
                    logger.error("error during update inventory.id="+inventoryId)
                    logger.error(error);
                    responseArray.push({"status":"failure","inventoryId":inventoryId,"message ":"error during update inventory. reason="+error});
                    resolve("done");
                }else{
                    logger.info("inventory updated successfully="+inventoryId);
                    responseArray.push({"status":"success","inventoryId":inventoryId,"message ":"inventory updated successfully"});
                    resolve("done");
                }
            }
            });
    }catch(ex){
        logger.error("error during update inventory="+inventoryId);
        responseArray.push({"status":"failure","inventoryId":inventoryId,"message ":"error during update inventory. reason="+ex});
        resolve("done");
    }
    },
    updatePriceFromFile:function(request,response){
        return new Promise(async function (resolve,reject) {
        logger.debug("in updatePriceFromFile method");
        var responseArray = [];
        try{
        var fileNameJsonMap  = orderUtil.readJsonFromXMLFile(config.priceFileProcessLocation,true,config.priceFileArchiveLocation,null);
        logger.debug("fileNameJsonMap size "+fileNameJsonMap.size);
        if(fileNameJsonMap.size > 0){
            for (var entry of fileNameJsonMap.entries()){
                var fileName = entry[0];
                var result = entry[1];
                logger.debug("filename {},result {}",fileName,JSON.stringify(result));
                if(result.item != null){
                    if(result.item.length > 0){
                        logger.debug("multiple price in single file="+fileName);
                        for(const priceItem of result.item){
                            await new Promise(resolve => catalogService.updatePriceStatusFromJson(priceItem, resolve, responseArray));
                        }
                    }else{
                        logger.debug("single price in file="+fileName);
                        await new Promise(resolve => catalogService.updatePriceStatusFromJson(result.item, resolve, responseArray));
                   }
                }
            }
            logger.info("all price update proccess completed",responseArray);
            response.status(200).json(responseArray);
            resolve(response);
        }else{
            response.status(200).json([{"status":"success","skuId":"","message ":"no price files to process"}]);
            resolve(response);
        }
    }catch(ex){
        logger.error("error while updating price")
        logger.error(ex);
        response.status(400).json([{"status":"failure","skuId":"","message ":"error while updating price. reason:"+ex}]);
        reject(response);
    }
    });

  },
  updatePriceStatusFromJson : function(priceJson, resolve, responseArray){
    var skuId = priceJson.$.materialNo;
    logger.debug("skuId="+skuId);
    if(skuId != null && skuId != undefined && skuId != ""){
        try{
            var priceData = catalogService.generatePriceDataForPriceItem(priceJson);
            logger.debug("priceData=",JSON.stringify(priceData));
            occClient.put({
                url: config.priceURL+skuId,data: priceData,
                headers: {
                    Accept: '*/*',
                    Connection: 'keep-alive'			 
                    },
                callback:function(error,res){
                    if(error){
                        logger.error("error during update price. sku id="+skuId)
                        logger.error(error);
                        responseArray.push({"status":"failure","skuId":skuId,"message ":"error during update price. reason="+error});
                        resolve("done");
                    }else{
                        logger.info("price updated successfully. skuid="+skuId);
                        responseArray.push({"status":"success","skuId":skuId,"message ":"price updated successfully"});
                        resolve("done");
                    }
                }
                });
        }catch(ex){
            logger.error("error during update price.skuid="+skuId);
            responseArray.push({"status":"failure","skuId":skuId,"message ":"error during update price. reason="+ex});
            resolve("done");
        }
    }else{
        logger.error("error during update price. skuid is null");
        responseArray.push({"status":"failure","skuId":skuId,"message ":"error during update price. skuid is null"});
        resolve("done");
    }
    },
    generatePriceDataForPriceItem:function(priceItem){
        var region = priceItem.$.region;
        var priceListId = config.US==region?config.priceListIdUS:config.priceListIdCA;
        var listPrice=priceItem.list != null && priceItem.list.price != null ? priceItem.list.price : null;
        var salePrice=priceItem.sale != null && priceItem.sale.price != null ? priceItem.sale.price : null;
        var priceData = {
              "listPrices":catalogService.getPriceJsonFromPrice(listPrice,priceListId),
              "salePrices":catalogService.getPriceJsonFromPrice(salePrice,priceListId)
            }
        return priceData;
    },
    getPriceJsonFromPrice(price,priceListId){
        if(price != null && price !=undefined){
            return {
                [priceListId]:price
            };
        } else
        {
            return null;
        }
    }
    /*updatePriceFromFile:function(request,response){
        return new Promise(async function (resolve,reject) {
        logger.debug("in updatePriceFromFile method");
        var responseArray = [];
        try{
        var fileNameJsonMap  = orderUtil.readJsonFromXMLFile(config.priceFileProcessLocation,false,config.priceFileArchiveLocation,null);
        logger.debug("fileNameJsonMap size "+fileNameJsonMap.size);
        if(fileNameJsonMap.size > 0){
            for (var entry of fileNameJsonMap.entries()){
                var fileName = entry[0];
                var result = entry[1];
                logger.debug("filename {},result {}",fileName,JSON.stringify(result));
                if(result.item != null){
                    var priceDataArray = {
                        "items":catalogService.generatePriceDataArrayFromJson(result.item)
                    }
                    //logger.debug("priceDataArray=",JSON.stringify(priceDataArray));
                    await new Promise(resolve => catalogService.updatePriceStatusForPriceDataArray(priceDataArray,fileName,resolve, responseArray));
                }
            }
            logger.info("all price update process completed",responseArray);
            response.status(200).json(responseArray);
            resolve(response);
        }else{
            response.status(200).json([{"status":"success","fileName":"","message ":"no price files to process"}]);
            resolve(response);
        }
        }catch(ex){
            logger.error("error while updating price")
            logger.error(ex);
            response.status(400).json([{"status":"failure","fileName":"","message ":"error while updating price. reason:"+ex}]);
            reject(response);
        }
    });
    },
    generatePriceDataArrayFromJson : function(priceJson){
        var priceJsonArray = [];
        if(priceJson.length > 0){
            logger.debug("multiple price in single file");
            for(const priceItem of priceJson){
                priceJsonArray.push(this.generatePriceDataForPriceItem(priceItem));
            }
        }else{
            logger.debug("single price in file");
            priceJsonArray.push(this.generatePriceDataForPriceItem(priceJson));
        }
        //logger.debug("pricejsonarray=",priceJsonArray);
        return priceJsonArray;
    },
    updatePriceStatusForPriceDataArray : function(priceDataArray, fileName, resolve, responseArray){
        try{
            occClient.put({
                url: config.priceURL,data: priceDataArray,
                headers: {
                    Accept: '**',
                    Connection: 'keep-alive'			 
                    },
                callback:function(error,res){
                    if(error){
                        returnStatus=false;
                        logger.error("error during update price")
                        logger.error(error);
                        movePriceFile(false, fileName, responseArray);
                        responseArray.push({"status":"failure","fileName":fileName,"message ":"error during update price. reason="+error});
                        resolve("done");
                    }else{
                        returnStatus=true;
                        logger.info("price updated successfully");
                        movePriceFile(true, fileName, responseArray);
                        responseArray.push({"status":"success","fileName":fileName,"message ":"price updated successfully"});
                        resolve("done");
                    }
                }
                });
        }catch(ex){
            logger.error("error during update price=");
            responseArray.push({"status":"failure","fileName":fileName,"message ":"error during update price. reason="+ex});
            resolve("done");
        }
        }*/
}
module.exports = catalogService;
function movePriceFile(success, fileName, responseArray) {
    try{
        if (success) {
            logger.info("moving price file to archive location");
            orderUtil.moveProcessedFile(config.priceFileProcessLocation + fileName, config.priceFileArchiveLocation + fileName);
        }
        else {
            logger.error("moving price file to error location");
            orderUtil.moveProcessedFile(config.priceFileProcessLocation + fileName, config.priceFileErrorLocation + fileName);
        }
    }catch(ex){
        logger.error("error during moving file");
        logger.error(ex);
        responseArray.push({"status":"failure","fileName":fileName,"message ":"error during moving file. reason="+ex});
    }
}

