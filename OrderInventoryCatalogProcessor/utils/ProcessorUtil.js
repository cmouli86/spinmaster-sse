'use strict';
var logger = require('./Logger');
var js2xmlparser = require("js2xmlparser");
var xml2jsparser = require("xml2js");
var dateFormat = require('date-format');
const fs = require('fs');
var config = require('../config/configuration.js');
var commercesdk = require('../occ/occ-sdk');
var occClient = new commercesdk(config.OCCConfig);
const request = require('superagent');
require('superagent-proxy')(request);

var processorUtil = {
    parseJsonToXML : function(jsonObj, count) {            
        logger.debug("test print gwp in order==="+jsonObj.gwp);
            var parsedXML = js2xmlparser.parse("orderSubmit",generateJsonForXML(jsonObj),{
            typeHandlers: {
                '[object Null]': function (value) {
                    return "";
                },
                '[object Undefined]': function (value) {
                    return "";
                }
            },
            declaration: {
                encoding: config.xmlEncoding, 
                standalone: config.xmlStandAlone,
                version:config.xmlVersion
            }
        });
        var returnMessage = processorUtil.writeXmlToFile(parsedXML, count);
        return returnMessage;
    },
    readJsonFromXMLFile:function (sourceLoc,moveFileAfterRead,destinationLoc,optionsMap){
        logger.debug("readJsonFromXMLFile called.inputs=",sourceLoc,destinationLoc,moveFileAfterRead,optionsMap);
        optionsMap = optionsMap != null ? optionsMap : new Map();
        var fileNameJsonMap = new Map();
        var filenames = fs.readdirSync(sourceLoc);
        logger.debug("filenames=",filenames);
        for(var i=0; i < filenames.length;i++){
            var fileContent = fs.readFileSync(sourceLoc + filenames[i], 'utf-8');
            xml2jsparser.parseString(fileContent, {trim: optionsMap.get('trim')||true, explicitArray : optionsMap.get('explicitArray')||false, 
                        explicitRoot  : optionsMap.get('explicitRoot')|| false}, function (err, result) {
                if(err){
                    logger.error("error during parsing input xml",err);
                } else {
                    //logger.debug("json="+JSON.stringify(result));
                    fileNameJsonMap.set(filenames[i],result);
                    }
                });
                if(moveFileAfterRead){
                    logger.debug("moving file="+filenames[i]);
                    var source = sourceLoc+filenames[i];
                    var dest = destinationLoc+filenames[i];
                    processorUtil.moveProcessedFile(source, dest);
                }
        }
        logger.debug("getJsonDataFromFiles fileNameJsonMap==",fileNameJsonMap);
        return fileNameJsonMap;
    },
    getOrderStatusJsonFromFiles : function(){
        logger.debug("getOrderStatusJsonFromFiles called");
        var fileNameJsonMap = new Map();
        var filenames = fs.readdirSync(config.orderStatusFileLocation);
        logger.debug("filenames=",filenames);
        for(var i=0; i < filenames.length;i++){
            var fileContent = fs.readFileSync(config.orderStatusFileLocation + filenames[i], 'utf-8');
            xml2jsparser.parseString(fileContent, {trim: true, explicitArray : false, explicitRoot  : false}, function (err, result) {
                if(err){
                    logger.error("error during parsing input xml",err);
                } else {
                    //logger.debug("json="+JSON.stringify(result));
                    fileNameJsonMap.set(filenames[i],result);
                    }
                });
                logger.debug("moving file="+filenames[i]);
                var source = config.orderStatusFileLocation+filenames[i];
                var dest = config.orderStatusArchiveLocation+filenames[i];
                processorUtil.moveProcessedFile(source, dest);
        }
        logger.debug("getJsonDataFromFiles fileNameJsonMap==",fileNameJsonMap);
        return fileNameJsonMap;
    },
    writeXmlToFile : function(parsedXML,count){
        var returnMessage = "success";
        logger.debug("writeXmlToFile called");
        logger.debug("parsedxml",parsedXML);
        var currDate = new Date();
        currDate.setSeconds(currDate.getSeconds() + count);
        var dateString = dateFormat.asString(config.orderSubmitXMLFileNameDateFormat,currDate);
        logger.debug("filename of xml="+config.xmlFileLocation+config.xmlFileName+dateString);
        try{
            fs.writeFileSync(config.xmlFileLocation+config.xmlFileName+dateString+'.xml', parsedXML);
            logger.debug("written to file in location==="+config.xmlFileLocation);
        }
        catch(error){
            logger.error("error during file write.");
            logger.error(error);
            returnMessage = "error during file write. please check if this location exists. location="+config.xmlFileLocation;
        }
        return returnMessage;
    },
    getFormattedDateString : function(inputDate,inputFormat,outputFormat) {
        logger.debug("getFormattedDateString called");
        var outputDateString = null;
        if(inputDate != null && inputDate != undefined){
            var parsedDate = dateFormat.parse(inputFormat, inputDate);
            outputDateString = dateFormat.asString(outputFormat, parsedDate);
        }
        return outputDateString;
    },
    moveProcessedFile : function(source, destination){
        var returnMessage = "success";
        logger.debug("moveProcessedFile called source {}, destination {}", source, destination);
        fs.renameSync(source, destination);
        logger.debug("moveProcessedFile completed");
        return returnMessage;
    }
 }

 function generateJsonForXML(jsonObj){
    logger.debug("generateJsonForXML called. order siteid="+jsonObj.siteId);
    var orderSiteId = jsonObj.siteId != null && jsonObj.siteId != undefined ? jsonObj.siteId : config.usSiteId;
    var generatedJson = {
        "order":{
            "@":{
                "atgProfileId":jsonObj.profileId,
                "region": config.usSiteId == orderSiteId ? config.usSite: config.caSite,
                "orderId":jsonObj.id,
                "site":config.orderSite
            },
            "billing":getBillingAddressJSON(jsonObj),
            "shipping":getShippingAddressJSON(jsonObj),
            "sapCustomerNumber":"",
            "thirdPartyShippingContract":config.thirdPartyShippingContract,
            "shippingCondition":config.shippingCondition,
            "specialInstructions1":"",
            "specialInstructions2":"",
            "paymentInfo":getPaymentInfoJSON(jsonObj,orderSiteId),
            "orderStatus":getOrderStatusJSON(jsonObj),
            "items":{
                "item":[
                    getLineItemsArray(jsonObj)
                ]
            }
        }
    };
    logger.debug("generatedjson="+JSON.stringify(generatedJson));
    return generatedJson;
 }

 function getOrderStatusJSON(jsonObj){
    logger.debug("getOrderStatusJSON called");
    var submittedDateString = processorUtil.getFormattedDateString(jsonObj.submittedDate, config.orderSubmitJsonDateFormat,config.orderSubmitDateFormat);
    var orderStatus = {
            "status":jsonObj.state,
            "orderSubmitDate":submittedDateString
    };
    return orderStatus;
}

 function getLineItemsArray(jsonObj){
     logger.debug("getLineItemsArray called");
     var lineItemsArray =  [];
     if(jsonObj.commerceItems != null && jsonObj.commerceItems.length > 0){
        jsonObj.commerceItems.forEach(element => {
            lineItemsArray.push(
                {
                    "@":{
                        "atgLineItemNo":element.id,
                        "materialNo":element.catalogRefId
                    },
                    "quantityType":config.quantityType,
                    "requestedQuantity":element.quantity,
                    "itemPrice":element.priceInfo.amount,
                    "freeItemFlag":element.x_isFreeToyAvailable ? 1 : 0
                }
            );
        });
    }
    return lineItemsArray;
 }
 
 function getPaymentInfoJSON(jsonObj, orderSiteId){
    logger.debug("getPaymentInfoJSON called")
     var paymentInfo = {};
     if(jsonObj.paymentGroups != null && jsonObj.paymentGroups.length > 0){
         jsonObj.paymentGroups.forEach(element => {
            paymentInfo = {
                "paymentType":config.paymentType,
                "creditCardCode":config.usSiteId == orderSiteId ? config.usCreditCardCode: config.caCreditCardCode,
                "totalPriceInfo":{
                    "@":{
                        "currency":jsonObj.priceInfo.currencyCode
                    },
                    "totalOrderAmount":jsonObj.priceInfo.total,
                    "totalShippingAmount":jsonObj.priceInfo.shipping,
                    "totalTaxAmount":jsonObj.priceInfo.tax,
                    "discounts":jsonObj.priceInfo.discountAmount
                }
            };
         });
     }
    return paymentInfo;
 }

 function getBillingAddressJSON(jsonObj){
     logger.debug("getBillingAddressJSON called")
     var billingJSON = {};
     if(jsonObj.paymentGroups != null && jsonObj.paymentGroups.length > 0){
        jsonObj.paymentGroups.forEach(element => {
            if(element.billingAddress != null){
                billingJSON = {
                    "@":{
                        "atgAddressId":""
                    },
                    "sapBillToNo":config.sapBillToNo,
                    "billToName":element.billingAddress.firstName,
                    "billToName2":element.billingAddress.lastName,
                    "billToCompany":element.billingAddress.companyName,
                    "billToAddr1":element.billingAddress.address1,
                    "billToAddr2":element.billingAddress.address2,
                    "billToCity":element.billingAddress.city,
                    "billToRegion":element.billingAddress.state,
                    "billToCountry":element.billingAddress.country,
                    "billToPostalCode":getFormattedPostalCode(element.billingAddress.postalCode,element.billingAddress.country),
                    "billToPhone":element.billingAddress.phoneNumber,
                    "billToEmail":element.billingAddress.email
                };
            }
        });
    }
    return billingJSON;
}

function getShippingAddressJSON(jsonObj){
    logger.debug("getShippingAddressJSON called")
    var shippingJSON = {};
    if(jsonObj.shippingGroups != null && jsonObj.shippingGroups.length > 0){
       jsonObj.shippingGroups.forEach(element => {
           if(element.shippingAddress != null){
               shippingJSON = {
                   "@":{
                       "atgAddressId":""
                   },
                   "sapShipToNo":config.sapShipToNo,
                   "shipToName":element.shippingAddress.firstName,
                   "shipToName2":element.shippingAddress.lastName,
                   "shipToCompany":element.shippingAddress.companyName,
                   "shipToAddr1":element.shippingAddress.address1,
                   "shipToAddr2":element.shippingAddress.address2,
                   "shipToCity":element.shippingAddress.city,
                   "shipToRegion":element.shippingAddress.state,
                   "shipToCountry":element.shippingAddress.country,
                   "shipToPostalCode":getFormattedPostalCode(element.shippingAddress.postalCode,element.shippingAddress.country),
                   "shipToPhone":element.shippingAddress.phoneNumber,
                   "shipToEmail":element.shippingAddress.email
               };
           }
       });
   }
   return shippingJSON;
}

function getFormattedPostalCode(postalCode, country){
    var formattedPostalCode = postalCode;
    if(postalCode != null && postalCode != undefined && postalCode != ''){
        if(config.usSite == country){
            formattedPostalCode = getPostalCodeForUS(postalCode);
        } else if(config.caSite == country){
            formattedPostalCode  = getPostalCodeForCA(postalCode);
        }
    }
    logger.debug("getFormattedPostalCode postalcode="+postalCode+" ,country="+country+
                    " ,formatted="+formattedPostalCode);
    return formattedPostalCode;
}

function getPostalCodeForUS(postalCode){
    postalCode =  postalCode.trim();
    var formattedPostalCode = postalCode;
    if(postalCode.length == 9){
        formattedPostalCode = postalCode.substring(0,5) + "-" + postalCode.substring(5,9);
    }else  if(postalCode.length == 10){
        formattedPostalCode = postalCode.substring(0,5) + "-" + postalCode.substring(6,10);
    }
    return formattedPostalCode;
}

function getPostalCodeForCA(postalCode){
    postalCode =  postalCode.trim();
    var formattedPostalCode = postalCode;
    if(postalCode.length == 6){
        formattedPostalCode = postalCode.substring(0,3) + " " + postalCode.substring(3,6);
    }else  if(postalCode.length == 7){
        formattedPostalCode = postalCode.substring(0,3) + " " + postalCode.substring(4,7);
    }
    return formattedPostalCode;
}
module.exports = processorUtil;