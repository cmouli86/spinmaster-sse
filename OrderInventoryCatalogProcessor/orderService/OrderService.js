const logger = require('../utils/Logger');
const config = require('../config/configuration.js');
var commercesdk = require('../occ/occ-sdk');
var occClient = new commercesdk(config.OCCConfig);
const orderUtil = require('../utils/ProcessorUtil.js');
var async = require("async");

var orderService = {
  createOrderXML:function(request,response){
    return new Promise(async function (resolve, reject) {
        var order = request.body != null ? request.body.order : null;
        var bodyOrderId = order != null && order.id != null && order.id != undefined && 
                        order.id != "" ? order.id : null; 
        var paramOrderId = request.query.orderId != null && request.query.orderId != undefined && 
                            request.query.orderId != "" ? request.query.orderId : null;
        var siteId =  order != null ? order.siteId : null;
        var orderState = order != null ? order.state : null;
        var validSiteIdToProcess = config.usSiteId == siteId || config.caSiteId == siteId ? true : false;
        var validOrderStateToProcess = config.submittedState == orderState ? true : false;
        logger.info("siteid and order id from request body= "+siteId +","+bodyOrderId
                        +" validsiteidtoprocess="+validSiteIdToProcess+" ,validOrderStateToProcess="+validOrderStateToProcess);
        if(bodyOrderId != null && validSiteIdToProcess && validOrderStateToProcess){
            try{
            var responseArray = [];
            logger.debug("in createOrderXML,request body has order json. converting it to xml");
            await new Promise(resolve => orderService.createOrderXMLForOrderId(bodyOrderId, resolve, responseArray, 0));
            response.status(200).json(responseArray);
            /*var message = orderUtil.parseJsonToXML(request.body.order,0);
            logger.debug("in createOrderXML, parseJsonToXML call completed. return message="+message);
            if("success" == message){
                orderService.updateOrderState(config.orderProcessingState,bodyOrderId);
                response.status(200).json([{"status":"success","orderId":bodyOrderId,"message ":"successfully written order xml"}]);
                resolve(response);
            } else{
                response.status(400).json([{"status":"failure","orderId":bodyOrderId,"message ":"error while creating and writing order xml. reason:"+message}]);
                reject(response);
            }*/
            }catch(ex){
                logger.error("error while creating and writing order xml.")
                logger.error(ex);
                response.status(400).json([{"status":"failure","orderId":bodyOrderId,"message ":"error while creating and writing order xml. reason:"+ex}]);
                reject(response);
         }
        } else 
        if(paramOrderId != null){
            logger.debug("taking orderids from query param and converting it to xml="+paramOrderId);
            var orderIds = paramOrderId.split(",");
            var responseArray = [];
            var count=0;
            for(const currentOrderId of orderIds){
                count++;
                //count variable to prevent override of file with same timestamp
                // due to parallel execution of order xml file creation
                logger.debug("currentorder id param="+currentOrderId+" count="+count);
                await new Promise(resolve => orderService.createOrderXMLForOrderId(currentOrderId, resolve, responseArray, count));
            }
            logger.info("order xml create proccess completed",responseArray);
            response.status(200).json(responseArray);
        }else{
            logger.error("no valid order in both parameter and request body");
            response.status(400).json([{"status":"failure","orderId":"","message ":"no order in both parameter and request body"}]);
            resolve(response);
        }
        });
    },

    createOrderXMLForOrderId:function(paramOrderId,resolve, responseArray, count){
        try{
        occClient.get({
            url: config.orderURL+paramOrderId,
            headers: {
                Accept: '*/*',
                Connection: 'keep-alive'			 
                },
            callback:function(err,res){
                if(err){
                logger.error("error during get order.id="+paramOrderId)
                logger.error(err);
                responseArray.push({"status":"failure","orderId":paramOrderId,"message ":"error while creating and writing order xml. reason:"+err});
                //response.status(400).json({"status":"failure","message ":"error while creating and writing order xml. reason:"+err});
                resolve("done");
                } else if(config.submittedState == res.state){
                    logger.debug("response order json generated for order="+paramOrderId);
                    //logger.debug(JSON.stringify(res));
                    var message = orderUtil.parseJsonToXML(res, count);
                    logger.debug("in createOrderXML, parseJsonToXML call completed. return message="+message);
                    if("success" == message){
                        //response.status(200).json({"status":"success","message ":"successfully written order xml"});
                        responseArray.push({"status":"success","orderId":paramOrderId,"message ":"successfully written order xml"});
                        resolve("done");
                        orderService.updateOrderState(config.orderProcessingState,paramOrderId);
                    } else{
                        responseArray.push({"status":"failure","orderId":paramOrderId,"message ":"error while creating and writing order xml. reason:"+message});
                        //response.status(400).json({"status":"failure","message ":"error while creating and writing order xml. reason:"+err});
                        resolve("done");
                    }
                }else{
                    logger.error("order state is not submmited. hence not creating order xml"+paramOrderId)
                    logger.error(err);
                    responseArray.push({"status":"failure","orderId":paramOrderId,"message ":"order state is not submitted. state="+res.state});
                    //response.status(400).json({"status":"failure","message ":"error while creating and writing order xml. reason:"+err});
                    resolve("done");
                }
            }
        });
    }catch(ex){
        logger.error("error during get order.id="+paramOrderId)
        logger.error(ex);
        responseArray.push({"status":"failure","orderId":paramOrderId,"message ":"error while creating and writing order xml. reason:"+ex});
        //response.status(400).json({"status":"failure","message ":"error while creating and writing order xml. reason:"+err});
        resolve("done");
    }
    },
    updateOrderStatus:function(request,response){
        return new Promise(async function (resolve,reject) {
        logger.debug("in updateOrderStatus method");
        var responseArray = [];
        try{
        var fileNameJsonMap  = orderUtil.getOrderStatusJsonFromFiles();
        logger.debug("fileNameJsonMap size "+fileNameJsonMap.size);
        if(fileNameJsonMap.size > 0){
            for (var entry of fileNameJsonMap.entries()){
                var fileName = entry[0];
                var result = entry[1];
                logger.debug("filename {},result {}",fileName,JSON.stringify(result));
                if(result.order != null){
                    if(result.order.length > 0){
                        logger.debug("multiple orders in single file="+fileName);
                        for(const currentOrder of result.order){
                            await new Promise(resolve => orderService.updateOrderStatusFromOrderJson(currentOrder, resolve, responseArray));
                        }
                    }else{
                        logger.debug("single order in file="+fileName);
                        await new Promise(resolve => orderService.updateOrderStatusFromOrderJson(result.order, resolve, responseArray));
                   }
                }
            }
            logger.info("all order update proccess completed",responseArray);
            response.status(200).json(responseArray);
            resolve(response);
        }else{
            response.status(200).json([{"status":"success","orderId":"","message ":"no files to process"}]);
            resolve(response);
        }
    }catch(ex){
        logger.error("error while updating order status")
        logger.error(ex);
        response.status(400).json([{"status":"failure","orderId":"","message ":"error while updating order status. reason:"+ex}]);
        reject(response);
    }
    });

  },
  updateOrderStatusFromOrderJson : function(orderJson, resolve, responseArray){
    var orderId = orderJson.$.id;
    var tracking = orderJson.tracking;
    var orderStatus = orderJson.status;
    logger.debug("order id="+orderId+" status="+orderStatus);
    try{
    if((config.shipped == orderStatus && tracking != null && tracking.track != null) 
        || config.cancelled == orderStatus){
        occClient.get({
            url: config.orderURL+orderId,
            headers: {
                Accept: '*/*',
                Connection: 'keep-alive'			 
                },
            callback:function(error,res){
                if(error){
                    logger.error("error during get order.id="+orderId)
                    logger.error(error);
                    responseArray.push({"status":"failure","orderId":orderId,"message ":"error during get order for this orderid. reason="+error});
                    resolve("done");
                }else{
                    if(config.cancelled == orderStatus){
                        res.state = config.orderCancelledState;
                        if(res.shippingGroups != null && res.shippingGroups.length > 0){
                                res.shippingGroups.forEach(element => {
                                    element.state = config.shipGroupCancelledState;
                            });
                        }   
                    } else {
                        var trackingNumber = [];
                        if(tracking.track.length > 0){
                            for(const trackObject of tracking.track){
                                trackingNumber.push(trackObject.carrier + "-" + trackObject.$.id);
                            }
                        } else {
                            trackingNumber.push(tracking.track.carrier + "-" + tracking.track.$.id);
                        }
                        var orderStatusDateString = orderUtil.getFormattedDateString(orderJson.orderStatusDate,config.orderStatusDateFormat,config.orderStatusJsonDateFormat);
                        logger.debug("trackingNumber {}, orderStatusDateString {}",trackingNumber.toString(),orderStatusDateString);
                        res.state = config.orderShippedState;
                        if(res.shippingGroups != null && res.shippingGroups.length > 0){
                        res.shippingGroups.forEach(element => {
                            element.trackingNumber = trackingNumber.toString();
                            element.actualShipDate = orderStatusDateString;
                            element.state = config.shipGroupShippedState;
                        });
                    }
                    
                    }
                    return orderService.callUpdateOrder(res, resolve, responseArray);
                }
            }
            });
         } else {
            logger.error("no shipping updates for order id="+orderId);
            responseArray.push({"status":"failure","orderId":orderId,"message ":"no shipping updates for this orderid"});
            resolve("done");
        }
    }catch(ex){
        logger.error("error during process order="+orderId);
        responseArray.push({"status":"failure","orderId":orderId,"message ":"error during process order. reason="+ex});
        resolve("done");
    }
    },
    callUpdateOrder:function(updatedJson, resolve, responseArray){
        let orderId = updatedJson.id;
        occClient.put({
            url: config.orderURL+orderId,data: updatedJson,
            callback: function(error,res){
                if(error){
                    logger.error("error during update order.id="+orderId)
                    logger.error(error);
                    responseArray.push({"status":"failure","orderId":orderId,"message ":"error during update order. reason="+error});
                    resolve("done");
                }else{
                    logger.info("order updated successfully="+orderId);
                    responseArray.push({"status":"success","orderId":orderId,"message ":"order updated successfully"});
                    resolve("done");
                }
            }
        });
    },
    updateOrderState:function(orderState, orderId){
        logger.info("updateOrderState order id="+orderId+", orderstate="+orderState);
        var updatedJson = {
            "state": orderState
        };
        occClient.put({
            url: config.orderURL+orderId,data: updatedJson,
            callback: function(error,res){
                if(error){
                    logger.error("error during updateOrderState for order.id="+orderId)
                    logger.error(error);
                }else{
                    logger.info("order state updated successfully");
                }
            }
        });
    }
}
module.exports = orderService;
